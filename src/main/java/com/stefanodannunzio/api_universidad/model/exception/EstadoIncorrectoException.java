package com.stefanodannunzio.api_universidad.model.exception;

public class EstadoIncorrectoException extends Throwable {

    public EstadoIncorrectoException(String message) {
        super(message);
    }
    
}
