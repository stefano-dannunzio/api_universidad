package com.stefanodannunzio.api_universidad.model.exception;

public class CorrelativasNoAprobadasException extends Throwable {
    
        public CorrelativasNoAprobadasException(String message) {
            super(message);
        }

}
