package com.stefanodannunzio.api_universidad.model.exception;

public class NotaIncorrectaException extends Throwable {
    
        public NotaIncorrectaException(String message) {
            super(message);
        }
    
}
