package com.stefanodannunzio.api_universidad.persistence.exception;

public class AlumnoNotFoundException extends Throwable {
    
    public AlumnoNotFoundException(String message) {
        super(message);
    }
}
