package com.stefanodannunzio.api_universidad.persistence.exception;

public class MateriaNotFoundException extends Throwable {
        
        public MateriaNotFoundException(String message) {
            super(message);
        }
    
}
