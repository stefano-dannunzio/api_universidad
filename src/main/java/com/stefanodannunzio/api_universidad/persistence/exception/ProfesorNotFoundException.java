package com.stefanodannunzio.api_universidad.persistence.exception;

public class ProfesorNotFoundException extends Throwable {
            
            public ProfesorNotFoundException(String message) {
                super(message);
            }
}
