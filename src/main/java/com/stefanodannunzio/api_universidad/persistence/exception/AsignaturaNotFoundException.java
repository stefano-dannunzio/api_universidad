package com.stefanodannunzio.api_universidad.persistence.exception;

public class AsignaturaNotFoundException extends Throwable {

    public AsignaturaNotFoundException(String message) {
        super(message);
    }
    
}
